﻿using searchfight.ProgramMng;

namespace searchfight
{
    class Program
    {
        static void Main(string[] args)
        {
            SearchFight manager = new SearchFight();
            manager.Run(args);
        }
    }
}
