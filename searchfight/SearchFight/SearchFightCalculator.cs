﻿using searchfight.Entities.SearchEngine;
using searchfight.Entities.SearchFight;
using System;
using System.Collections.Generic;

namespace searchfight.ProgramMng
{
    class TotalCalculator
    {
        Dictionary <string, SearchTotal> bestValues = new Dictionary<string, SearchTotal>();

        string[] words;

        List<ISearchEngine> searchEngines;

        public TotalCalculator(string [] words, List<ISearchEngine> searchEngines)
        {
            this.words = words;

            this.searchEngines = searchEngines;
        }

        public void CalculateTotal()
        {
            foreach (String word in words)
            {
                Console.Write($" {word} : ");

                long wordTotal = 0;

                foreach (ISearchEngine searchEngine in searchEngines)
                {
                    string response = searchEngine.MakeApiRequest(word);
                    long searchEngineTotal = searchEngine.GetTotalResult(response);

                    SearchTotal result = new SearchTotal(word, searchEngineTotal);
                    SetValue(searchEngine.Name, result);

                    wordTotal += searchEngineTotal;

                    Console.Write($" {searchEngine.Name} : {searchEngineTotal}");
                }
                
                SearchTotal totalResult = new SearchTotal(word, wordTotal);
                SetValue("Total", totalResult);

                Console.WriteLine();

            }
        }

        public void PrintBestValues()
        {
            foreach (KeyValuePair<string, SearchTotal> entry in bestValues)
            {
                Console.WriteLine($" {entry.Key} winner: {entry.Value.word}");
            }
        }
        
        private void SetValue(string searchEngine, SearchTotal result)
        {
            
            if(bestValues.ContainsKey(searchEngine))
            {
                SearchTotal bestPair = bestValues[searchEngine];

                if (bestPair.value >= result.value) return;
            }

            bestValues[searchEngine] = result;
            
        }
    }
}
