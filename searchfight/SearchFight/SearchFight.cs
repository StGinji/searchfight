﻿using searchfight.Entities.SearchEngine;
using searchfight.SearchEngine;
using System;
using System.Collections.Generic;

namespace searchfight.ProgramMng
{
    class SearchFight
    {
        
        public void Run(String [] args)
        {

            if (!VerifyArguments(args)) return;

            List<ISearchEngine> searchEngines = InitSearchEngines();

            TotalCalculator calculator = new TotalCalculator(args, searchEngines);

            calculator.CalculateTotal();

            calculator.PrintBestValues();
            
            Console.Read();

        }

        private Boolean VerifyArguments(String [] args)
        {
            if(args.Length < 1)
            {
                Console.WriteLine("You must enter at least one word.");
                Console.WriteLine("e.g. searchfight.exe java .net");
                return false; 
            }

            return true;
            
        }

        private List<ISearchEngine> InitSearchEngines()
        {
            List<ISearchEngine> searchEngines = new List<ISearchEngine>();

            GoogleSearchEngine googleSearchEngine = new GoogleSearchEngine();
            searchEngines.Add(googleSearchEngine);

            BingSearchEngine bingSearchEngine = new BingSearchEngine();
            searchEngines.Add(bingSearchEngine);

            return searchEngines;
        }
        
    }
}
