﻿
namespace searchfight.ProgramMng
{
    class BestValue
    {
        public string word { get; set; }
        public long bestValue { get; set; }


        public BestValue(string word, long value)
        {
            this.word = word;
            this.bestValue = value;
        }
    }
}
