﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Configuration;
using searchfight.Entities.Bing;
using searchfight.Entities.SearchEngine;

namespace searchfight.SearchEngine
{
    class BingSearchEngine : ISearchEngine
    {
        string name;

        string apiUrl;

        string apiKey;

        public string Name { get => name; set => name = value; }

        public BingSearchEngine()
        {
            var appSettings = ConfigurationManager.AppSettings;

            name = appSettings["BingName"];
            
            apiUrl = appSettings["BingApiUrl"];

            apiKey = appSettings["BingApiKey"];
        }
        
        public string MakeApiRequest(string word)
        {
            string html = string.Empty;

            String requestUrl = apiUrl + "?q=" + word;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
            request.Headers.Add("Ocp-Apim-Subscription-Key", apiKey);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
                html = reader.ReadToEnd();
            
            return html;
        }

        public long GetTotalResult(string response)
        {
            if (response.Equals(string.Empty)) return -1;

            BingResult result = JsonConvert.DeserializeObject<BingResult>(response);

            return result.webPages.totalEstimatedMatches;
        }


    }
}
