﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Configuration;
using searchfight.Entities.Google;
using searchfight.Entities.SearchEngine;

namespace searchfight.SearchEngine
{
    class GoogleSearchEngine : ISearchEngine
    {

        string name;

        string apiUrl;

        string arguments;

        public string Name { get => name; set => name = value; }

        public GoogleSearchEngine()
        {
            var appSettings = ConfigurationManager.AppSettings;

            name = appSettings["GoogleName"];

            apiUrl = appSettings["GoogleApiUrl"];

            String apiKey = appSettings["GoogleApiKey"];
            String searchEngineId = appSettings["GoogleSearchEngineId"];

            arguments = "?key=" + apiKey + "&cx=" + searchEngineId;
        }
        
        public string MakeApiRequest(string word)
        {
            string html = string.Empty;

            String requestUrl = apiUrl + arguments + "&q=" + word;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            html = reader.ReadToEnd();
            
            return html;
        }
        
        public long GetTotalResult(string response)
        {
            if (response.Equals(string.Empty)) return -1;

            GoogleResult result = JsonConvert.DeserializeObject<GoogleResult>(response);
            
            return result.searchInformation.totalResults;
        }
        
    }
}
