﻿using searchfight.SearchEngine;
using System;
using System.Collections.Generic;

namespace searchfight.ProgramMng
{
    class TotalCalculator
    {
        Dictionary <string, BestValue> bestValues = new Dictionary<string, BestValue>();

        string[] words;

        List<ISearchEngine> searchEngines;

        public TotalCalculator(string [] words, List<ISearchEngine> searchEngines)
        {
            this.words = words;

            this.searchEngines = searchEngines;
        }

        public void CalculateTotal()
        {
            foreach (String word in words)
            {
                Console.Write($" {word} : ");

                long wordTotal = 0;

                foreach (ISearchEngine searchEngine in searchEngines)
                {
                    string response = searchEngine.MakeApiRequest(word);
                    long searchEngineTotal = searchEngine.GetTotalResult(response);

                    SetValue(word, searchEngine.Name, searchEngineTotal);

                    wordTotal += searchEngineTotal;

                    Console.Write($" {searchEngine.Name} : {searchEngineTotal}");
                }

                SetValue(word, "Total", wordTotal);

                Console.WriteLine();

            }
        }

        public void PrintBestValues()
        {
            foreach (KeyValuePair<string, BestValue> entry in bestValues)
            {
                Console.WriteLine($" {entry.Key} winner: {entry.Value.word}");
            }
        }
        
        private void SetValue(string word, string searchEngine, long value)
        {
            
            if(bestValues.ContainsKey(searchEngine))
            {
                BestValue bestPair = bestValues[searchEngine];

                if (bestPair.bestValue < value)
                {
                    bestPair.bestValue = value;
                    bestPair.word = word;
                }
                
            }
            else
            {
                BestValue newValue = new BestValue(word, value);
                bestValues[searchEngine] = newValue;
            }
        }
    }
}
