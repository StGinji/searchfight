﻿
namespace searchfight.Entities.SearchFight
{
    public class SearchTotal
    {
        public string word { get; set; }
        public long value { get; set; }


        public SearchTotal(string word, long value)
        {
            this.word = word;
            this.value = value;
        }
    }
}
