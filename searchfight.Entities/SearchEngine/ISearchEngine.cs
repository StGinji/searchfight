﻿
namespace searchfight.Entities.SearchEngine
{
    public interface ISearchEngine
    {

        string Name { get; set; }

        string MakeApiRequest(string word);

        long GetTotalResult(string response);

    }
}
