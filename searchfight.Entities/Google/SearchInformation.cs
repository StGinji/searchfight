﻿
namespace searchfight.Entities.Google
{
    public class SearchInformation
    {
        public long searchTime { get; set; }
        public double formattedSearchTime0 { get; set; }
        public long totalResults { get; set; }
        public string formattedTotalResults { get; set; }
    }
}
